package ro.orangetraining;
import java.util.*;
import java.util.Comparator;
import java.util.stream.Stream;
import java.util.stream.Collectors;


public class Main {

    public static void main(String[] args) {
	//
        List<Employee> lst = new ArrayList<>();
        Employee e1 = new Employee("Jon Age", 22);
        lst.add(e1);
        Employee e2 = new Employee("Steve Age", 19);
        lst.add(e2);
        Employee e3 = new Employee("Kevin Age", 23);
        lst.add(e3);
        //Creem o lista cu rezultatele filtrarii;
        /*List<Employee> result=lst.stream().filter((p->p.getAge()>22)).collect(Collectors.toList());
        result.forEach(System.out::println);*/
        Employee result1=lst.stream()
                .filter((p->p.getAge()>23))
                .findAny()
                .orElse(null);
        System.out.println(result1);


    }
}
